package com.cgi.osd.student.paymentserver.rest;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Singleton;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

@Singleton
public class ClientProvider {

    private Client client;

    public Client getClient() {
	return this.client;
    }

    @PreDestroy
    private void cleanup() {
	if (this.client != null) {
	    this.client.close();
	}
    }

    @PostConstruct
    private void init() {
	this.client = ClientBuilder.newClient();
    }

}
