package com.cgi.osd.student.paymentserver.rest;

import java.util.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import com.cgi.osd.student.paymentserver.logic.PaymentLogic;
import com.cgi.osd.student.paymentserver.soap.PaymentFault_Exception;
import com.cgi.osd.student.paymentserver.soap.PaymentRequestDTO;
import com.cgi.osd.student.paymentserver.soap.PaymentResponseDTO;

@RequestScoped
@Path("/paymentservices")
@Produces("application/json")
@Consumes("application/json")
public class PaymentRestService {

    @Inject
    private PaymentLogic paymentLogic;

    @Inject
    private Logger logger;

    @Inject
    private ClientProvider clientProvider;

    @POST
    @Path("/pay")
    public PaymentResponseDTO performPurchase(PaymentRequestDTO request) throws PaymentFault_Exception {
	final PaymentResponseDTO response = this.paymentLogic.createPayment(request);
	return response;
    }

    @GET
    @Path("test")
    public void testPayment() {
	final PaymentRequestDTO request = new PaymentRequestDTO();
	request.setAmount(123.75);
	request.setCardHolderName("Sven Gardin");
	request.setCardNumber("1234");
	request.setRequestId("38daaa77-9c7d-42cf-8f10-d237f7d01f4e");

	final Client client = this.clientProvider.getClient();
	final WebTarget webTarget = client.target("http://127.0.0.1:8080/paymentserver/rest/paymentservices/pay");
	final PaymentResponseDTO response = webTarget.request(MediaType.APPLICATION_JSON).post(Entity.json(request),
		PaymentResponseDTO.class);
	this.logger.info("Got response " + response.getTransactionId());
    }

}
