package com.cgi.osd.student.paymentserver.soap;

import java.util.logging.Logger;

import javax.inject.Inject;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;

import org.apache.cxf.annotations.SchemaValidation;
import org.apache.cxf.annotations.SchemaValidation.SchemaValidationType;

import com.cgi.osd.student.paymentserver.logic.PaymentLogic;

/**
 * This class is responsible for implementing the payment service.
 *
 */
@SchemaValidation(type = SchemaValidationType.BOTH)
@WebService(endpointInterface = "com.cgi.osd.student.paymentserver.soap.PaymentServicePortType", targetNamespace = "http://soap.paymentserver.student.osd.cgi.com", name = "PaymentServicePortType", serviceName = "PaymentService", portName = "PaymentServiceSOAP", wsdlLocation = "PaymentService.wsdl")
@XmlSeeAlso({ ObjectFactory.class })
public class PaymentServiceBean implements PaymentServicePortType {

    @Inject
    private Logger logger;

    @Inject
    private PaymentLogic paymentLogic;

    @Override
    public PaymentResponseDTO performPurchase(PaymentRequestDTO request) throws PaymentFault_Exception {
	logger.fine("performPurchase successfully invoked.");
	final PaymentResponseDTO response = paymentLogic.createPayment(request);
	return response;
    }

}
