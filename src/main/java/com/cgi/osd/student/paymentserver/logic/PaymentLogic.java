package com.cgi.osd.student.paymentserver.logic;

import java.util.GregorianCalendar;
import java.util.UUID;

import javax.enterprise.context.ApplicationScoped;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.cgi.osd.student.paymentserver.soap.PaymentFault;
import com.cgi.osd.student.paymentserver.soap.PaymentFault_Exception;
import com.cgi.osd.student.paymentserver.soap.PaymentRequestDTO;
import com.cgi.osd.student.paymentserver.soap.PaymentResponseDTO;

@ApplicationScoped
public class PaymentLogic {

    public PaymentResponseDTO createPayment(PaymentRequestDTO request) throws PaymentFault_Exception {
	try {
	    final GregorianCalendar gregorianCalendar = new GregorianCalendar();
	    DatatypeFactory datatypeFactory;
	    datatypeFactory = DatatypeFactory.newInstance();
	    final XMLGregorianCalendar now = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);

	    final double amount = request.getAmount();
	    final String requestId = request.getRequestId();
	    final UUID transactionUUID = UUID.randomUUID();
	    final String transactionId = transactionUUID.toString();

	    final PaymentResponseDTO response = new PaymentResponseDTO();
	    response.setAmount(amount);
	    response.setRequestId(requestId);
	    response.setTransactionId(transactionId);
	    response.setTransactionTime(now);

	    return response;
	} catch (final DatatypeConfigurationException e) {
	    final PaymentFault paymentFault = new PaymentFault();
	    paymentFault.setFaultDescription(e.getMessage());
	    final PaymentFault_Exception paymentFaultException = new PaymentFault_Exception("Server error",
		    paymentFault);
	    throw paymentFaultException;
	}
    }

}
